from __future__ import print_function
import pickle
import os.path
import glob
# Raise exception if < Python 3.5

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

spreadsheetID = '10dgFgpPha4HKs0juPYTdhDa934CpgSrpGDGCHF38tJk'
sheetID = 0

def readFromMacro(file, columns):

    splitPath = file.split(sep = '/')

    # Isotope
    isotopesER = ['Co60', 'K40', 'Th232', 'U238', 'Sc46', 'Rn220', 'Rn222', 'Xe127']
    if any(isotope in file for isotope in isotopesER):
        interactionType = 'ER'
        isotope = next((i for i in isotopesER if i in splitPath[-1]), None)
        if isotope is None:
            print('Isotope not found for file ' + file)
            return None
    else:
        interactionType = 'NR'
        isotope = 'neutron'

    # That's all the information that can be extracted from the filename.
    # Now actually read the file.
    with open(file, 'r') as f:
        lineList = f.readlines()
        generatorLines = [line for line in lineList if '/source/set' in line]
        if len(generatorLines) == 0:
            print('Generator line is not found for file ' + file)
            return None

        listOfData = []

        for line in generatorLines:
            splitGeneratorLine = line.strip().split(sep = ' ')

            if interactionType == 'ER':
                if len(splitGeneratorLine) <= 6:
                    volumesSpecified = True
                    generatorCommand, location, *sourceGeneratorList = splitGeneratorLine
                    fancyTypesetLocation = separateTheLocationString(location)
                    sampleName = isotope + ' ' + fancyTypesetLocation 
                    macroType = 'generic'
                else: # No location is specified - coordinates are given instead
                    volumesSpecified = False 
                    generatorCommand, *sourceGeneratorList = splitGeneratorLine
                    location = ' '.join(sourceGeneratorList[1:4])
                    fancyTypesetLocation = '-- Set by positions --'
                    sampleName = isotope + ' ' + fancyTypesetLocation 
                    macroType = 'hardcoded'
                sourceGeneratorString = ' '.join(sourceGeneratorList).strip()
                sourceActivity = ' '.join(sourceGeneratorString.split(sep = ' ')[-2:])
            else:
                if len(splitGeneratorLine) <= 8:
                    volumesSpecified = True
                    generatorCommand, location, *sourceGeneratorList = splitGeneratorLine
                    fancyTypesetLocation = separateTheLocationString(location)
                    sampleName = isotope + '_' + fancyTypesetLocation 
                    macroType = 'generic'
                else:
                    volumesSpecified = False 
                    generatorCommand, *sourceGeneratorList = splitGeneratorLine
                    sourceGeneratorList = [word for word in sourceGeneratorList if word != ''] # Deal with inconsistent spacing between words in the macro
                    location = ' '.join(sourceGeneratorList[1:4]) # Get the XYZ positions
                    fancyTypesetLocation = '-- Set by positions --'
                    sampleName = isotope + ' ' + fancyTypesetLocation 
                    macroType = 'hardcoded'

                sourceGeneratorString = ' '.join(sourceGeneratorList)
                location = location + ' - multiple activation values'
                sourceActivity = 'Multiple values'

            # LUXsim or BACCARAT
            generatorSoftware = generatorCommand.split(sep = '/')[1]

            values = [
                isotope,
                fancyTypesetLocation,
                sampleName,
                macroType,
                generatorSoftware,
                'N/A',
                'N/A',
                location,
                sourceGeneratorString,
                sourceActivity,
                file
            ]

            listOfData.append(dict(zip(columns, values)))

    return listOfData

def separateTheLocationString(string):
    underscoreSeparated = string.split('_')
    if len(underscoreSeparated) == 1:
        # Usual acronymns in volume names that should not be separated
        if 'PMT' in underscoreSeparated[0]:
            return 'PMT ' + ''.join(' ' + char if char.isupper() else char.strip() for char in underscoreSeparated[0].strip('PMT')).strip()
        elif 'PTFE' in underscoreSeparated[0]:
            return 'PTFE ' + ''.join(' ' + char if char.isupper() else char.strip() for char in underscoreSeparated[0].strip('PTFE')).strip()
        else:
            return ''.join(' ' + char if char.isupper() else char.strip() for char in underscoreSeparated[0]).strip()
    else:
        return ' '.join(underscoreSeparated).strip()

def setupSheet():
    creds = None
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port = 0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('sheets', 'v4', credentials = creds)
    return service

def writeToSheet(columns):
    service = setupSheet()
    sheet = service.spreadsheets()

    # The following line will only work if the number of columns is less than 28, which is the case now
    rangeToWrite = 'Sheet1!A1:' + chr(ord('@') + len(columns)) + '1'
    body = {
        'range': rangeToWrite,
        'majorDimension': "ROWS",
        'values': [ columns ]# Column headers 
    }

    result = sheet.values().update(
        spreadsheetId = spreadsheetID,
        valueInputOption = 'USER_ENTERED', 
        range = body.get('range'),
        body = body).execute()

    return sheet

def updateSheet(macroData, sheet):

    # values = [{'userEnteredValue': {'stringValue': value}} for value in macroData.values()]
    values = [[cellData for cellData in rowData.values()] for rowData in macroData]

    valueRange = {
        'range': 'Sheet1!A2:' + chr(ord('@') + len(values[0])) + str(len(values) + 1),
        'majorDimension': "ROWS",
        'values': values
    }

    requestBody = {
        'valueInputOption': "USER_ENTERED",
        'data': valueRange,
        'includeValuesInResponse': False,
        'responseValueRenderOption': "FORMATTED_VALUE",
        'responseDateTimeRenderOption': "FORMATTED_STRING"
    }

    request = sheet.values().batchUpdate(
        spreadsheetId = spreadsheetID,
        body = requestBody
    )
    response = request.execute()

    return 

def formatSheet(sheet):
    sheet.batchUpdate(
        spreadsheetId = spreadsheetID,
        body = {'requests': [
            {
                'repeatCell': {
                    'range': {
                        'sheetId': sheetID,
                        'startRowIndex': 0,
                        'endRowIndex': 1
                    },
                    'cell': {
                        'userEnteredFormat': {
                            'textFormat': {
                                'bold': True 
                            }
                        }
                    },
                    'fields': 'userEnteredFormat(textFormat)'
                }
            },
            {
                'updateSheetProperties': {
                    'properties': {
                        'sheetId': sheetID,
                        'gridProperties': {
                            'frozenRowCount': 1,
                            'frozenColumnCount': 2
                        }
                    },
                    'fields': 'gridProperties(frozenRowCount,frozenColumnCount)'
                }
            },
            {
               'autoResizeDimensions': {
                  'dimensions': {
                     'sheetId': sheetID,
                     'dimension': "COLUMNS",
                     'startIndex': 0,
                     'endIndex': 20 
                  } 
               } 
            }
        ]}).execute()

def getUniqueData(macroData):
    listOfSampleNames = []
    uniqueData = []
    for data in macroData:
        sampleName = data.get('Isotope') + data.get('BACCARAT location')
        if sampleName not in listOfSampleNames:
            listOfSampleNames.append(sampleName)
            uniqueData.append(data)
    return uniqueData

def main():

    columnsToWrite = [
        'Isotope', 
        'Location',
        'Sample Name (Isotope_Location)',
        'Macro type',
        'Generation Software',
        'BACCARAT version',
        'LZLAMA version',
        'BACCARAT location',
        'BACCARAT generator',
        'BACCARAT source activity',
        'Filepath'
    ]

    sheet = writeToSheet(columnsToWrite) # Writes column headers 

    pathToMacros= 'BackgroundMacros'
    macroFiles = glob.glob(pathToMacros + '/**/*.mac', recursive = True)

    macroData = []
    for file in macroFiles:
        data = readFromMacro(file, columnsToWrite)
        if data is not None:
            macroData.extend(data)
    macroData = getUniqueData(macroData) # remove duplicates
    updateSheet(macroData, sheet)
    formatSheet(sheet)

    return
    
if __name__ == '__main__':
    main()